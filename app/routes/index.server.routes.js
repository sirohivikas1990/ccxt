module.exports = function(app) {
    var index = require('../controllers/index.server.controller');
    app.get('/', index.render);
    app.route('/coin-price').get(index.getCoinInformation);
    app.route('/exchange-info').get(index.getCoinExchangeInformation);
    app.route('/fetch-excange-data').get(index.fetchExchangeData);
	
};