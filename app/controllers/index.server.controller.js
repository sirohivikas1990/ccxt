
'use strict';

var ccxt = require('ccxt');
var async = require("async");
var mysql      = require('mysql');
var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : '',
      database : 'ccxt'
});

connection.connect();


exports.render = function(req, res) {
    res.render('index', {
        title: 'CryptoMarket API',
        user: req.user ? req.user.username : ''

    });
};  
  

exports.getCoinInformation = function(req, res, next) 
{
    connection.query('SELECT * from market_data where market_name="'+req.query.pair+'"', function (error, results, fields) {
      if (error) res.json({"status": "NOTOK","errorcode":1001,"errormessage" : "Coin pair not found","messagedetail": "Provide more info if needed"});
      res.json({"ok":true,"Results":results});
    })

};

exports.getCoinExchangeInformation = function(req, res, next) 
{
    connection.query('SELECT * from market_data where exchange="'+req.query.name+'"', function (error, results, fields) {
      if (error) res.json({"status": "NOTOK","errorcode":1002,"errormessage" : "Invalid exchange not found","messagedetail": "Provide more info if needed"});
      res.json({"ok":true,"Results":results});  
    })
};

exports.fetchExchangeData = function(req, res, next) 
{
    
    var type = req.query.cryptocurrencytype;
    console.log(type);
   
    (async function main () {
        var MarketsDataSet;
        if(type == 'bittrex'){
            let bittrex    = new ccxt.bittrex ()
            MarketsDataSet = await bittrex.loadMarkets ()
        }  

        if(type == 'poloniex'){
           let poloniex  = new ccxt.poloniex ()
           MarketsDataSet = await poloniex.loadMarkets ()
        }
  
        async.each(MarketsDataSet, function(marketdata, callback) {

            connection.query('INSERT INTO market_data (exchange, amount_min, amount_max, price_min,price_max, precision_amount,precision_price,tierbased,percentage,maker,taker,id,symbol,base,quotes,active,market_currency,base_currency,market_currency_long,base_currency_long,min_trade_size,market_name,is_active,created,lot) VALUES ("'+type+'",'+marketdata.limits.amount.min+',0,0,0,'+marketdata.precision.amount+','+marketdata.precision.price+','+marketdata.tierBased+','+marketdata.percentage+','+marketdata.maker+','+marketdata.taker+',"'+marketdata.id+'","'+marketdata.symbol+'","'+marketdata.base+'","'+marketdata.quote+'",'+marketdata.active+',"'+marketdata.info.MarketCurrency+'","'+marketdata.info.BaseCurrency+'","'+marketdata.info.MarketCurrencyLong+'","'+marketdata.info.BaseCurrencyLong+'",'+marketdata.info.MinTradeSize+',"'+marketdata.info.MarketName+'",'+marketdata.info.IsActive+',"'+marketdata.info.Created+'","'+marketdata.lot+'")', function (error, results, fields) {
              if (error) throw error;
               callback();
            });  

        }, function(err) {
           if( err ) {
              res.json({"ok":false,"Messgae":"Process failed to insert exchange data in the db"});
            } else {
              console.log('All the exchange data have been processed successfully');
              connection.query('SELECT * from market_data', function (error, results, fields) {
                if (error) throw error;
                res.json({"ok":true,"Results":results});
              });
           
            } 
        });
        
}) ()

   

};




