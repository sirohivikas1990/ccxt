var port = process.env.PORT || 3000;

module.exports = {
    port: port,
    db:'mongodb://localhost:27017/shop360'
};
