var config = require('./config'),
 	bodyParser = require('body-parser'),
	express = require('express');
	//passport = require('passport'),
	flash = require('connect-flash'),
	session = require('express-session');

module.exports = function() 
{
    var app = express();

    app.use(bodyParser.urlencoded({
        extended: true
    }));

    app.use(bodyParser.json());

    //app.use(passport.initialize());
	//app.use(passport.session());
	
	/*app.use(session({
    	saveUninitialized: true,
    	resave: true,
    	secret: 'OurSuperSecretCookieSecret'
	}));
*/
	app.use(flash());

    app.set('views', './app/views');
	app.set('view engine', 'ejs');

    require('../app/routes/index.server.routes.js')(app);
	/*require('../app/routes/user.server.routes.js')(app);
	require('../app/routes/shops.server.routes.js')(app);
    require('../app/routes/product.server.routes.js')(app);*/
    
    app.use(express.static('./public'));

    return app;
};  